import oerplib
import time
import sys
import os

url = sys.argv[1] if len(sys.argv) > 1 else 'web'

database_password = os.environ.get('DB_PASSWORD', 'admin')
database_name = os.environ.get('ODOO_DATABASE', 'default')
master_password = os.environ.get('MASTER_PASSWORD', "admin")

print "Connect ", url
oerp = oerplib.OERP(server=url, protocol='xmlrpc', port=8069)
oerp.config['timeout'] = 600

print "Get databases"
databases = oerp.db.list()

print databases

if database_name not in databases :
  print "Create Database"
  oerp.db.create_database(master_password, database_name, True, 'en_US', database_password)
  time.sleep(10)

print "login"
oerp.login(user='admin', passwd=database_password, database=database_name)
module_obj = oerp.get('ir.module.module')

print "Update modules list"
update_module_obj = oerp.get('base.module.update')
updage_module_instance = update_module_obj.create({})
update_module_obj.update_module(updage_module_instance)

modules = [
  'product',
  'sale_management',
  'stock',
  'hr',
'purchase',
  'account',
  'website_sale',
]

for module_name in modules:
  module_id = module_obj.search([('name', '=', module_name)])

  if len(module_id) > 0:
    tries = 0
    while True:  
      try:
        tries += 1
        module = module_obj.browse(module_id[0])
        if module.state == "installed":
          print "update module " + module_name
          module_obj.button_immediate_upgrade(module_id)
        else:
          print "install module " + module_name
          module_obj.button_immediate_install(module_id)          
      except Exception as e:
        if tries >= 5:
          raise e
      else:
        break
